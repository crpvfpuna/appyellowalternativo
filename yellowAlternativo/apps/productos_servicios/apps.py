from django.apps import AppConfig


class ProductosServiciosConfig(AppConfig):
    name = 'productos_servicios'
